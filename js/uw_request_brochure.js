/**
 * @file
 * Javascript for request brochure
 */

(function ($) {

Drupal.behaviors.exampleModule = {
	attach: function (context, settings) {
		$("#international").hide();
		$("#edit-studenttype").change(function (){
			switch($(this).val()){
				case "1":
				$("#international").hide();
				$("#other").hide();
				$("#canadian").show();
				break;
				case "2":
				$("#international").show();
				$("#other").hide();
	 			$("#canadian").hide();
				break;
				case "3":
				$("#international").hide();
				$("#other").show();
				$("#canadian").hide();
				break;
			}
		});
	}
};




/*

function validateSubmit() {
	if ($('#studentType').val() == 3)
	{
		alert ('"Not Currently Attending a Secondary School" options do not work yet');
		return false;
	}
	if ($("input[name='system']:checked").length || $('#studentType').val() == 1)
	{
		return true;
	}
	return false;
}
$(document).ready(function(){
	$("#canadian").click(function(){
		alert("hello");
	});
	$("input[type='radio']#system").click(function (){
		$("#country").val("");
	});
	$("#country").change(function (){
		$("input[type='radio']:checked").attr('checked', false);
		$.ajax({
			type:'GET',
			url: 'systemajax.php',
			data: {country: $('#country').val() },
			success: function (data) {
				var radioOptions = '';
				$.each(data, function () {
					radioOptions+="<input type='radio' name='system' id='system' value='"+this.system_id+"' /> "+this.name+"<br />";
					$('#systems-list').html(radioOptions);
				});
				if (data.length == 1) { //Only 1 system choice
					$("#system").attr('checked', true);
					$('#toggleSystems').hide();
					//Perhaps I could hide the whole 'system section', however since the radio button still exists and is checked the value will be sent on submit
				}
				else {
					$('#toggleSystems').show();
				}
			}
		});
	});
	if ($('#country').val() != '') {
		$('#country').change();
	}
	$("#international").hide();
	$("#other").hide();
	$("#studentType").val("1");
	$("#studentType").change(function (){
		switch($(this).val()){
		case "1":
		$("#international").hide();
		$("#other").hide();
		$("#canadian").show();
		break;
		case "2":
		$("#international").show();
		$("#other").hide();
	 	$("#canadian").hide();
		break;
		case "3":
		$("#international").hide();
		$("#other").show();
		$("#canadian").hide();
		break;
		}
	});
});


*/
})(jQuery);
