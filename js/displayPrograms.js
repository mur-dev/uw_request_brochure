/**
 * @file
 * Javascript for request brochure
 */

(function ($) {
	Drupal.behaviors.exampleModule = {
	attach: function (context, settings) {
		$('#expand').click(function () {
			$('#expanded').slideToggle("fast");
		});

		if ($('input[name=orderType]:checked').val() == 'digital') {
			$('.mail-only').hide();
		}

		$('#edit-ordertype').change(function() {
			if ($('input[name=orderType]:checked').val() == 'digital') {
				$('.mail-only').hide();
			}
			else {
				$('.mail-only').show();
			}
		})

		$('form').submit(function() {
			$('.form-submit').prop("disabled", true);
		})
	}
	};

})(jQuery);

jQuery(function($) {
  var requiredTag = ' <abbr class="form-required" title="This field is required.">* <span>(required)</span></abbr>';
	$(document).ready(function() {
	  if ($('#edit-leadtype').val() == 'counsellor' || $('#edit-leadtype').val() == '') {
		jQuery('.startyear').hide();
	  }
	  else if ($('#edit-leadtype').val() == 'parent'){
		jQuery('.startyear').show();
		jQuery("label[for='edit-startyear']").html('When does your son/daughter plan to begin university?' + requiredTag);
	  }
	  else {
		jQuery('.startyear').show();
		jQuery("label[for='edit-startyear']").html('When do you plan to begin university?' + requiredTag);
	  }
	});
	jQuery('#edit-leadtype').change(function() {
	  if (this.value == 'counsellor' || this.value == '') {
		jQuery('.startyear').hide();
	  }
	  else if (this.value == 'parent'){
		jQuery('.startyear').show();
		jQuery("label[for='edit-startyear']").html('When does your son/daughter plan to begin university?' + requiredTag);
	  }
	  else {
		jQuery('.startyear').show();
		jQuery("label[for='edit-startyear']").html('When do you plan to begin university?' + requiredTag);
	  }
	});
});
